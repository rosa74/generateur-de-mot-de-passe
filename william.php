<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Generateur</title>
</head>

<body>
    <?php
    $lcLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    $ucLetters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    $numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    $specials =  ['&', '~', '#', '%', '$', '*', '@', '/'];


    $length = isset($_GET['length']) ? intval($_GET['length']) : 0;
    $hasSpecials = isset($_GET['specials']);
    $hasUC = isset($_GET['upper']);
    $hasNumber = isset($_GET['numbers']);

    $totalLetters = [];
    $totalLetters = array_merge($totalLetters, $lcLetters);

    if ($hasUC) {
        $totalLetters = array_merge($totalLetters, $ucLetters); // ajoute les maj
    }

    if ($hasNumber) {
        $totalLetters = array_merge($totalLetters, $numbers); // ajoute les nombres
    }

    if ($hasSpecials) {
        $totalLetters = array_merge($totalLetters, $specials); // ajoute les specs
    }



    function generatePasswd($letters, $length)
    {
        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $password .= randomLetter($letters);
        }
        return $password;
    } // génère un mot de passe avec les caractères de $letters et de longueur $length

    function randomLetter($letters)
    {
        return $letters[rand(0, sizeof($letters) - 1)];
    } // retourne une lettre aléatoire du tableau $letters

    // echo randomLetter($numbers);
    echo generatePasswd($totalLetters, $length);

    ?>
    <form action="" method="get">
        <div>
            <label for="length">longueur du mdp</label><br>
            <input id="length" type="number" name="length" min="1" max="100" value="<?php echo $length ?>">
        </div>
        <div>
            <label><input type="checkbox" name="upper" <?php if ($hasUC) echo 'checked' ?>>Majuscules</label>
        </div>
        <div>
            <label><input type="checkbox" name="numbers" <?php if ($hasNumber) echo 'checked' ?>Nombres</label> </div> <div>
                <label><input type="checkbox" name="specials" <?php if ($hasSpecials) echo 'checked' ?>Caractères spéciaux</label> </div> <input type="submit">
    </form>

<?php
$lowerCaseLetters = "azertyuiopqsdfghjklmwxcvbn";
$upperCaseLetters = strtoupper($lowerCaseLetters);
$digits = "0123456789";
$specials = '&~#^$-_/!@%*';

function randomLetter($string)
{
    $length = strlen($string);
    $randomNumber = rand(0, $length - 1);
    return $string[$randomNumber];
}

function generatePassword($letters, $length)
{
    $password = '';
    for ($i = 0; $i < $length; $i++) {
        $password .= randomLetter($letters);
    }
    return $password;
}

if (isset($_GET['length']) && $_GET['length']>0) {
    
        $letters = $lowerCaseLetters;
        if (isset($_GET['upper'])) {
            $letters .= $upperCaseLetters;
        }
        if (isset($_GET['digits'])) {
            $letters .= $digits;
        }
        if (isset($_GET['specials'])) {
            $letters .= $specials;
        }
        $passwd = generatePassword($letters, intval($_GET['length']));
    ?>
    <hr>
    <h1>Votre mot de passe</h1>
    <p><?= $passwd ?></p>
    <hr>
    <?php
}
function checked($name) {
    if (isset($_GET[$name])){
        return 'checked';
    } else {
        return '';
    }
}
?>
    <form action="" method="get">
        <div>
            <input type="number" name="length" min="1" max="100" value="<?= isset($_GET['length'])?$_GET['length']:0 ?>">
        </div>
        <div>
            <label><input type="checkbox" name="upper" <?= checked('upper') ?>>Majuscules</label>
        </div>
        <div>
            <label><input type="checkbox" name="digits" <?= checked('digits') ?>>Nombres</label>
        </div>
        <div>
            <label><input type="checkbox" name="specials" <?= checked('specials') ?>>Caractères spéciaux</label>
        </div>
        <input type="submit">
    </form>
<?php



    <!--
    <form action='william.php' method='GET'>

        <input type="number" name="lenght" min="1" max="100" value="<?php echo $length ?>">

        <div>
            <label> <input type="checkbox" name="upper" <?php if ($hasUC) echo 'checked' ?> >Majuscules</label>
        </div>

        <div>
            <label> <input type="checkbox" name="numbers" <?php if ($hasNumber) echo 'checked' ?>>Nombres</label>
        </div>

        <div>
            <label> <input type="checkbox" name="specials" <?php if ($hasSpecials) echo 'checked' ?>>Caracteres speciaux</label>
        </div>

        <input type="submit" value='Envoyer'>
        --!>


        <?php
        /*
        $lettresMinuscules = ["a", "z", "e", "r", "t", "y", "u", "i", "o", "p", "q", "s", "d", "f", "g", "h", "j", "k", "l", "m", "w", "x", "c", "v", "b", "n"];
        $lettresMajuscules = ["A", "Z", "E", "R", "T", "Y", "U", "I", "O", "P", "Q", "S", "D", "F", "G", "H", "J", "K", "L", "M", "W", "X", "C", "V", "B", "N"];
        $nombres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        $caracteresSpeciaux = ["&", "~", "#", "%", "$", "*", "@", "/"];

        $length = isset($_GET['length']) ? intval($_GET['length']) : 0;
        $hasSpecials = isset($_GET['specials']);
        $hasUC = isset($_GET['upper']);
        $hasNumber = isset($_GET['numbers']);

        $totalLetters = [];
        $totalLetters = array_merge($totalLetters, $lettresMinuscules);
        $totalLetters = array_merge($totalLetters, $lettresMajuscules);
        $totalLetters = array_merge($totalLetters, $nombres);
        $totalLetters = array_merge($totalLetters, $caracteresSpeciaux);


        function generatePasswd($letters, $length)
        {

            $password = " ";

            for ($i = 0; $i < $length; $i++) {

                $password .= randomLetter($letters);
            }
            return $password;
        }

        function randomLetter($letters)
        {

            return $letters[rand(0, sizeof($letters) - 1)];
        }
        echo  generatePasswd($totalLetters, $length);*/

        ?>



</body>

</html>